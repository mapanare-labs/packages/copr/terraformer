%define debug_package %{nil}

Name:           terraformer
Version:        0.8.24
Release:        1%{?dist}
Summary:        CLI tool to generate terraform files from existing infrastructure (reverse Terraform). Infrastructure to Code
Group:          Applications/System
License:        ASL 2.0
URL:            https://www.gruntwork.io/
Source0:        https://github.com/GoogleCloudPlatform/%{name}/archive/refs/tags/%{version}.tar.gz
Source1:        https://github.com/GoogleCloudPlatform/%{name}/releases/download/%{version}/terraformer-all-linux-amd64

%description
CLI tool to generate terraform files from existing 
infrastructure (reverse Terraform). Infrastructure to Code

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.8.24

* Sat Sep 24 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.8.22

* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
